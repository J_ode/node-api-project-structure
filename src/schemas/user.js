const {
    Schema,
    model
} = require("mongoose"); // Assumes package is installed

const User = new Schema({
    username: {
        type: String,
        required: true,
        unique: true,
        trim: true
    },
    password: {
        type: String,
        required: true,
        min: 8
    }
}, {
    timestamps: true
});

module.exports = model("user", User);