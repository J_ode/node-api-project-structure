/**
 * import all schemas here
 */
const user = require("./user");


/** Another export option
 *  Attach the schemas to the index module
 *  All schema props should be lowercase to allow the mapper match them  
 */
const index = {}
index.user = user;

/**
 * Use to get single schema
 * returns a single instance of the schema
 */
exports.mapper = function (name) {
    return index[name];
}

/**
 * Export the index module
 * Allows for the independent use for the schemas
 */
module.exports = index;