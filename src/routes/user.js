/**
 * Assumes packages are installed
 * (express)
 */
const express = require('express');
const router = express.Router();
const {
    user
} = require("../controllers")

router.get("/users/:id", user.getUser);


module.exports = router;