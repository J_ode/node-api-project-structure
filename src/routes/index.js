const user = require("./user");

/**
 * The export here can used
 * mounted app.js middleware in app.user()
 * entity loader in starup directory
 * or middleware mounter inside the middleware directory
 */

module.exports = {
    user
}