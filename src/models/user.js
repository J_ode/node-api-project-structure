const {
    mapper
} = require("../schemas");

/**
 * You write all you user model related queries like
 * find user, delete user, update user details etc,
 * mongoose suports find,findOne, findByIdAndUpdate, findByIdAndDelete etc 
 */


/**
 * Find a user by Id
 * returns the user object
 */
exports.find = function find(id) {
    const user = mapper("user"); // give access to the mongoose model in the schema directory
    return new Promise(async (resolve, reject) => {
        try {
            const respoonse = await user.find(id).exec();
            if (response == null) { // this is mongoose specific, postgres would return an empty array
                // another option ==> reject(new Error("User not found.")) 
                reject(respoonse)
            }
            resolve(respoonse);
        } catch (error) {
            // another option ==> reject(new Error("An error has occured."))
            reject(null)
        }
    })
}