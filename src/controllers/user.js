const {
    findUser
} = require("../services/user");
const {
    http
} = require("../utilities")
const {
    status,
    responseMessage
} = require("../config");

/**
 * No business logic should happen here
 * All business logic should be handled by the appropriate services
 * Only checks and responses are handled here
 */
exports.getUser = function (req, res) {
    const id = req.params.id;
    try {
        const user = await findUser(id);
        if (user == null) { // this should be based on you model implementation
            return http.error(
                res,
                status.clientError.notFound,
                responseMessage.user.USER_NOT_FOUND
            );
        }
        return http.response(
            res,
            status.successful.ok,
            responseMessage.user.USER_NOT_FOUND,
            user
        );
    } catch (error) {
        return http.error(
            res,
            status.serverError.internalServerError,
            responseMessage.error.SERVER_ERROR // optionally the error mesaage (error.message)
        );
    }
}