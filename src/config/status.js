/**
 * All HTTP status codes
 * This could be added to constant file
 */
const STATUS = {};

STATUS.successful = {
    ok: 200,
    created: 201
}

STATUS.clientError = {
    badRequest: 400,
    unAuthorized: 401,
    fobidden: 403,
    notFound: 401
}

STATUS.serverError = {
    internalServerError: 500,
    notImplemented: 501
}

module.exports = STATUS;