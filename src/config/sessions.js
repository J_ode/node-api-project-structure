/**
 * Inject the dependecies where it is used or 
 * inside index.js file of the config directory
 * before exporting the config file
 * 
 * The dependencies can be passed as an object or spread parameters
 * { secret, cokie:{ cookies props }, store: {instace of mongooseconnection }}
 */
module.exports = function sessions(secret, cookie, store) {
    return {
        secret, // process.env.SESSION_SECRET || config.configx.SESSION_SECRET,
        cookie, // { maxAge: 1000 * 60 * 60 }, // 1hrs
        resave: false,
        saveUninitialized: false,
        store, // new MongoStore({ mongooseConnection: mongoose.connection })
    };
}