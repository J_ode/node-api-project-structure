/**
 * These are the bear minimum when using mongoose
 * It could be replace with any ORM package or mongoDB SDK or 
 * any other databse configurations
 */
exports.devConfig = function config() {
    return {
        useCreateIndex: true,
        useNewUrlParser: true,
        useFindAndModify: false,
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
}

exports.stagingConfig = function stagingConfig() {
    return {
        // staging configurations
        // maybe from cloud source like mongoDB Atlas 
    }
}

exports.prodConfig = function prodConfig() {
    return {
        // production configurations
        // maybe from 
        // mongoDB Community Edition install on your remote server
        // cloud source like mongoDB Atlas
    }
}