const cor = {
    origin: '*',
    method: ['GET', 'PUT', 'POST', 'DELETE'], // Only support methods
    optionsSuccessStatus: 200,
    exposedHeaders: ['Content-Range', 'X-Content-Range', 'Access-Control-Allow-Headers: Accept'] // Extend to fit your needs
};


module.exports = cors;