/**
 * Add all entity related query response here
 */


exports.user = {
    USER_NOT_FOUND: "User not found.",
    USER_LOGGIN: "User successfully logged in."
}

exports.error = {
    SERVER_ERROR: "We are currently experiencing technical issues please try again later.",
    AUTH_ERROR: "You are not authorized to perform this action",
    AUTH_FAILED: "User authorization failed."
}