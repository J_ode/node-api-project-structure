/**
 * I have included different export pattern.
 * Use your prefered method and be consistent
 */
const dbConfig = require("./db");
const cors = require("./cors");
const sessions = require("./sessions");
const responseMessage = require("./responseMessages");
const status = require("./status");

module.exports = {
    dbConfig,
    cors,
    sessions,
    responseMessage,
    status
}