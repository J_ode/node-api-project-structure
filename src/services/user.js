/**
 * This is the file where you use the models
 * for your database.
 * 
 * This is where you business logic should reside
 * Create all actions required by user controller
 * to access your data model hear. This services
 * are used by the controll.
 * 
 */
const {
    find
} = require("../models/user");

/**
 * This is a simple implementation 
 * Choose your own style is you want
 */
exports.findUser = function (userId) {
    return new Promise(async (resolve, reject) => {
        try {
            const user = await find(userId);
            if (user == null) {
                reject(user)
            }
            resolve(user);
        } catch (error) {
            reject(error.message);
        }
    });
}