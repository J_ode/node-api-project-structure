/**
 * res ==> express response object
 * status ==> http status code (100 - 500)
 * statusCode ==> response status (error,successful,failed)
 * message ==> state representation of the action (User not found, Payment could not be process. Please try later etc.)
 * data ==> payload of the reponse ( array of data or single object)
 */

exports.response = function (res, statusCode, status, message, data) {
    return res.status(statusCode).json({
        status,
        message,
        data
    });
}

exports.error = function (res, statusCode, status, message) {
    return res.status(statusCode).json({
        status,
        message
    });
}